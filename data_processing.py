import glob
import json
import os
import random
import xml.etree.ElementTree as ET
from logging import getLogger
from typing import Dict, List

import click
from tqdm import tqdm

from utils.misc import write2file, str2bool

logger = getLogger(__name__)


def get_label2id(labels_path: str) -> Dict[str, int]:
    """id is 1 start"""
    with open(labels_path, 'r') as f:
        labels_str = f.read().split()
    labels_ids = list(range(1, len(labels_str) + 1))
    return dict(zip(labels_str, labels_ids))


def get_annpaths(ann_dir_path: str = None,
                 ann_ids_path: str = None,
                 ext: str = '',
                 annpaths_list_path: str = None) -> List[str]:
    # If use annotation paths list
    if annpaths_list_path is not None:
        with open(annpaths_list_path, 'r') as f:
            ann_paths = f.read().split()
        return ann_paths

    # If use annotaion ids list
    ext_with_dot = '.' + ext if ext != '' else ''
    with open(ann_ids_path, 'r') as f:
        ann_ids = f.read().split()
    ann_paths = [os.path.join(ann_dir_path, aid + ext_with_dot) for aid in ann_ids]
    return ann_paths


def get_image_info(annotation_root, extract_num_from_imgid=True, ann_root=None):
    path = annotation_root.findtext('path')
    # if path is None:
    filename = ann_root.replace("xml", "jpg")
    filename = os.path.basename(filename)
    img_id = filename.split(".")[0]
    # if extract_num_from_imgid and isinstance(img_id, str):
    #     img_id = int(re.findall(r'\d+', img_id)[0])

    size = annotation_root.find('size')
    width = int(size.findtext('width'))
    height = int(size.findtext('height'))

    image_info = {
        'file_name': filename,
        'height': height,
        'width': width,
        'id': img_id
    }
    return image_info


def get_coco_annotation_from_obj(obj, label2id):
    # label = obj.findtext('name')
    # print(label)
    # assert label in label2id, f"Error: {label} is not in label2id !"
    # category_id = label2id[label]
    category_id = 1
    bndbox = obj.find('bndbox')
    xmin = int(bndbox.findtext('xmin')) - 1
    ymin = int(bndbox.findtext('ymin')) - 1
    xmax = int(bndbox.findtext('xmax'))
    ymax = int(bndbox.findtext('ymax'))
    assert xmax > xmin and ymax > ymin, f"Box size error !: (xmin, ymin, xmax, ymax): {xmin, ymin, xmax, ymax}"
    o_width = xmax - xmin
    o_height = ymax - ymin
    ann = {
        'area': o_width * o_height,
        'iscrowd': 0,
        'bbox': [xmin, ymin, o_width, o_height],
        'category_id': category_id,
        'ignore': 0,
        'segmentation': []  # This script is not for segmentation
    }
    return ann


def convert_xmls_to_cocojson(annotation_paths: List[str],
                             label2id: Dict[str, int],
                             output_jsonpath: str,
                             extract_num_from_imgid: bool = True):
    output_json_dict = {
        "images": [],
        "type": "instances",
        "annotations": [],
        "categories": []
    }
    bnd_id = 1  # START_BOUNDING_BOX_ID, TODO input as args ?
    print('Start converting !')
    for a_path in tqdm(annotation_paths):
        # Read annotation xml
        ann_tree = ET.parse(a_path)
        ann_root = ann_tree.getroot()

        img_info = get_image_info(annotation_root=ann_root,
                                  extract_num_from_imgid=extract_num_from_imgid, ann_root=a_path)
        img_id = img_info['id']
        output_json_dict['images'].append(img_info)
        for obj in ann_root.findall('object'):
            ann = get_coco_annotation_from_obj(obj=obj, label2id=label2id)
            ann.update({'image_id': img_id, 'id': bnd_id})
            output_json_dict['annotations'].append(ann)
            bnd_id = bnd_id + 1

    for label, label_id in label2id.items():
        category_info = {'supercategory': 'none', 'id': label_id, 'name': label}
        output_json_dict['categories'].append(category_info)

    with open(output_jsonpath, 'w') as f:
        output_json = json.dumps(output_json_dict)
        f.write(output_json)


@click.command()
@click.option('--ann_dir', type=str,
              help='path to annotation files directory. It is not need when use --ann_paths_list')
@click.option('--ann_ids', type=str,
              help='path to annotation files ids list. It is not need when use --ann_paths_list')
@click.option('--ann_paths_list', type=str,
              help='path of annotation paths list. It is not need when use --ann_dir and --ann_ids')
@click.option('--labels', type=str,
              help='path to label list.')
@click.option('--output', type=str, default='shop.json', help='path to output json file')
@click.option('--ext', type=str, default='', help='additional extension of annotation file')
def generate_coco_dataset(ann_dir, ann_ids, ann_paths_list, labels, output, ext):
    label2id = get_label2id(labels_path=labels)
    ann_paths = get_annpaths(
        ann_dir_path=ann_dir,
        ann_ids_path=ann_ids,
        ext=ext,
        annpaths_list_path=ann_paths_list
    )
    convert_xmls_to_cocojson(
        annotation_paths=ann_paths,
        label2id=label2id,
        output_jsonpath=output,
        extract_num_from_imgid=True
    )
    logger.info("Generating coco-format successfully!!")


@click.command()
@click.option("--data_dir", default="./data/shop/")
@click.option("--ann_dir", default="annotations", type=str)
@click.option("--ann_file", default="annpaths_list.txt", type=str)
@click.option("--trainval", default=True, type=str2bool)
@click.option("--test_ratio", default=0.2, type=float)
def prepare_dataset(data_dir, ann_dir, ann_file, trainval, test_ratio):
    path_list = glob.glob(os.path.join(data_dir, ann_dir, "*.xml"))
    with open(os.path.join(data_dir, ann_file), "w") as f:
        for i, path in enumerate(path_list):
            if i < len(path_list) - 1:
                path += "\n"
            f.write(path)

    random.shuffle(path_list)

    path_list = [path.split("/")[-1].split(".")[0] for path in path_list]

    if trainval:
        train_file = os.path.join(data_dir, "dataset_ids", "trainval.txt")
        write2file(path_list, train_file)
    else:
        train_file = os.path.join(data_dir, "dataset_ids", "train.txt")
        val_file = os.path.join(data_dir, "dataset_ids", "val.txt")

        val_list = path_list[:int(len(path_list) * test_ratio)]
        train_list = path_list[int(len(path_list) * test_ratio):]

        write2file(train_list, train_file)
        write2file(val_list, val_file)

    logger.info("Finished !!!")


@click.group()
def cli():
    pass


if __name__ == '__main__':
    cli.add_command(prepare_dataset)
    cli.add_command(generate_coco_dataset)
    cli()
