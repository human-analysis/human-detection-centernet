from unittest import TestCase

import torch

from datasets.data_factory import get_dataset


class OPT:
    def __init__(self):
        self.data_dir = "./data/"
        self.trainval = True
        self.kitti_split = '3dop'
        self.keep_res = True
        self.pad = 31
        self.input_h = 512
        self.intput_w = 512
        self.not_rand_crop = True
        self.scale = 0.4
        self.shift = 0.1
        self.flip = 0.5
        self.no_color_aug = True
        self.down_ratio = 4
        self.mse_loss = False
        self.hm_gauss = None
        self.dense_wh = True
        self.cat_spec_wh = True
        self.reg_offset = True
        self.debug = 0


class Unittest(TestCase):
    def test_coco_format(self):
        Dataset = get_dataset("shop", "ctdet")
        opt = OPT()
        dataset = Dataset(opt, 'train')

    def test_data_loader(self):
        Dataset = get_dataset("shop", "ctdet")
        opt = OPT()
        dataset = Dataset(opt, 'train')
        train_loader = torch.utils.data.DataLoader(
            dataset,
            batch_size=2,
            shuffle=False,
            num_workers=1,
            pin_memory=True
        )
        for i, batch in enumerate(train_loader):
            print(batch)
            break
