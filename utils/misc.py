def str2bool(v):
    return v.lower() in ("yes", "true", "t", "1")


def write2file(list, file):
    with open(file, "w") as f:
        for i, path in enumerate(list):
            if i < len(list) - 1:
                path += "\n"
            f.write(path)
