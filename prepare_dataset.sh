python3 data_processing.py prepare-dataset \
        --data_dir ./data/shop/ \
        --ann_dir annotations \
        --ann_file annpaths_list.txt \
        --trainval False \
        --test_ratio 0.2

python3 data_processing.py generate-coco-dataset \
        --ann_dir data/shop/annotations/ \
        --ann_ids data/shop/dataset_ids/train.txt \
        --ann_paths_list data/shop/annpaths_list.txt \
        --labels data/shop/labels.txt \
        --output data/shop/shop_trainval.json

python3 data_processing.py generate-coco-dataset \
        --ann_dir data/shop/annotations/ \
        --ann_ids data/shop/dataset_ids/val.txt \
        --ann_paths_list data/shop/annpaths_list.txt \
        --labels data/shop/labels.txt \
        --output data/shop/shop_test.json