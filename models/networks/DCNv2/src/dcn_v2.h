#pragma once

#include "cpu/vision.h"
#include <torch/script.h>
#ifdef WITH_CUDA
// #include "cuda/vision.h"
#include "cuda/vision_ext.h"
#endif

at::Tensor
dcn_v2_forward(const at::Tensor &input,
               const at::Tensor &weight,
               const at::Tensor &bias,
               const at::Tensor &offset,
               const at::Tensor &mask,
               const int64_t kernel_h,
               const int64_t kernel_w,
               const int64_t stride_h,
               const int64_t stride_w,
               const int64_t pad_h,
               const int64_t pad_w,
               const int64_t dilation_h,
               const int64_t dilation_w,
               const int64_t deformable_group)
{
    if (input.type().is_cuda())
    {
#ifdef WITH_CUDA
        return dcn_v2_cuda_forward( input, weight, bias, offset, mask,
                                    static_cast<int>(kernel_h), 
                                    static_cast<int>(kernel_w),
                                    static_cast<int>(stride_h), 
                                    static_cast<int>(stride_w),
                                    static_cast<int>(pad_h), 
                                    static_cast<int>(pad_w),
                                    static_cast<int>(dilation_h), 
                                    static_cast<int>(dilation_w),
                                    static_cast<int>(deformable_group));
#else
        AT_ERROR("Not compiled with GPU support");
#endif
    }
    AT_ERROR("Not implemented on the CPU");
}

std::vector<at::Tensor>
dcn_v2_backward(const at::Tensor &input,
                const at::Tensor &weight,
                const at::Tensor &bias,
                const at::Tensor &offset,
                const at::Tensor &mask,
                const at::Tensor &grad_output,
                int64_t kernel_h, int64_t kernel_w,
                int64_t stride_h, int64_t stride_w,
                int64_t pad_h, int64_t pad_w,
                int64_t dilation_h, int64_t dilation_w,
                int64_t deformable_group)
{
    if (input.type().is_cuda())
    {
#ifdef WITH_CUDA
        return dcn_v2_cuda_backward(input,
                                    weight,
                                    bias,
                                    offset,
                                    mask,
                                    grad_output,
                                    static_cast<int>(kernel_h), 
                                    static_cast<int>(kernel_w),
                                    static_cast<int>(stride_h), 
                                    static_cast<int>(stride_w),
                                    static_cast<int>(pad_h), 
                                    static_cast<int>(pad_w),
                                    static_cast<int>(dilation_h), 
                                    static_cast<int>(dilation_w),
                                    static_cast<int>(deformable_group));
#else
        AT_ERROR("Not compiled with GPU support");
#endif
    }
    AT_ERROR("Not implemented on the CPU");
}

std::tuple<at::Tensor, at::Tensor>
dcn_v2_psroi_pooling_forward(const at::Tensor &input,
                             const at::Tensor &bbox,
                             const at::Tensor &trans,
                             const int64_t no_trans,
                             const double spatial_scale,
                             const int64_t output_dim,
                             const int64_t group_size,
                             const int64_t pooled_size,
                             const int64_t part_size,
                             const int64_t sample_per_part,
                             const double trans_std)
{
    if (input.type().is_cuda())
    {
#ifdef WITH_CUDA
        return dcn_v2_psroi_pooling_cuda_forward(input,
                                                 bbox,
                                                 trans,
                                                 static_cast<int>(no_trans),
                                                 static_cast<float>(spatial_scale),
                                                 static_cast<int>(output_dim),
                                                 static_cast<int>(group_size),
                                                 static_cast<int>(pooled_size),
                                                 static_cast<int>(part_size),
                                                 static_cast<int>(sample_per_part),
                                                 static_cast<float>(trans_std));
#else
        AT_ERROR("Not compiled with GPU support");
#endif
    }
    AT_ERROR("Not implemented on the CPU");
}

std::tuple<at::Tensor, at::Tensor>
dcn_v2_psroi_pooling_backward(const at::Tensor &out_grad,
                              const at::Tensor &input,
                              const at::Tensor &bbox,
                              const at::Tensor &trans,
                              const at::Tensor &top_count,
                              const int64_t no_trans,
                              const double spatial_scale,
                              const int64_t output_dim,
                              const int64_t group_size,
                              const int64_t pooled_size,
                              const int64_t part_size,
                              const int64_t sample_per_part,
                              const double trans_std)
{
    if (input.type().is_cuda())
    {
#ifdef WITH_CUDA
        return dcn_v2_psroi_pooling_cuda_backward(out_grad,
                                                  input,
                                                  bbox,
                                                  trans,
                                                  top_count,
                                                  static_cast<int>(no_trans),
                                                  static_cast<float>(spatial_scale),
                                                  static_cast<int>(output_dim),
                                                  static_cast<int>(group_size),
                                                  static_cast<int>(pooled_size),
                                                  static_cast<int>(part_size),
                                                  static_cast<int>(sample_per_part),
                                                  static_cast<float>(trans_std));
#else
        AT_ERROR("Not compiled with GPU support");
#endif
    }
    AT_ERROR("Not implemented on the CPU");
}
